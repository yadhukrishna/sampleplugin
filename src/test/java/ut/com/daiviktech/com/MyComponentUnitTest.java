package ut.com.daiviktech.com;

import org.junit.Test;
import com.daiviktech.com.api.MyPluginComponent;
import com.daiviktech.com.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}